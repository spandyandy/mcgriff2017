# README #



There are two parts to this project. The first is to learn how to use MapReduce, using simple implementations and progressing through the WebMapReduce framework developed jointly between Macalester College and St. Olaf University, to the large-scale version designed by Google and other companies that exploit large datasets.

After understanding the basics of MapReduce, we seek to determine how this system might be used on smaller, inexpensive multicore processor systems.
We would also like to research Phoenix++ interfaces to MapReduce, with the goal of implementing a small-scale MapReduce system on the Raspberry Pi.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

* scarl@sewanee.edu
* kimj0@sewanee.edu